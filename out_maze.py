from robot_control_class import RobotControl
import time

robotcontrol = RobotControl()

class MoveRobot:

    def __init__(self, motion, clockwise, speed, time):
        self.robot = RobotControl()
        self.motion = motion
        self.clockwise = clockwise
        self.speed = speed
        self.time = time
        self.time_turn = 2.5
        self.full_laser = self.robot.get_laser_full()
        self.full_laser2 = self.robot.get_laser_full()
        self.laser = self.robot.get_laser(360)

    def out_maze(self):
        corner = 0
        while (corner < 2):
            self.laser = self.robot.get_laser(360)
        while (self.laser > 1):
            self.move_straight()
            self.laser = self.robot.get_laser(360)
            print("Current distance to this wall is %f meters girando direita" % self.laser)
            self.robot.stop_robot()
            self.turn()
            self.robot.stop_robot()
            corner+=1

    def door_maze(self):
        door = 0
        while (door <2):
            self.laser = self.robot.get_laser(360)
        while (self.laser > 1):
            self.move_straight()
            self.laser = self.robot.get_laser(360)
            print("Current distance to wall is %f meters girando esquerda" % self.laser)
            self.robot.stop_robot()
            self.turn()
            self.robot.stop_robot()
            door+=1

    def move_straight_time(self):
        self.robot.move_straight_time(self.motion, self.speed, self.time)

    def move_straight(self):
        self.robot.move_straight()

    def turn(self):
        self.robot.turn(self.clockwise, self.speed, self.time_turn)

    def stop_robot(self):
        self.robot.stop_robot()

move_inside = MoveRobot('forward', 'clockwise', 0.75, 2.5)
move_inside.out_maze()
move_outside = MoveRobot('forward', 'counterclockwise', 0.75, 2.5)
move_outside.door_maze()